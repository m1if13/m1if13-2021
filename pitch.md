# Solid Rain

Dans un futur plus ou moins lointain, la Terre est frappée par une pluie de météorites faites de différents métaux actuellement inconnus sur terre. Ces météorites, de formes et de tailles étrangement similaires, atterrissent au sol avec une régularité étonnante. Pourtant, les métaux qu’elles contiennent ne sont pas toujours les mêmes…

<img width="100%" height="200px" src="./meteors.jpg" alt="Pluie de météorites, Nasa">

Les derniers survivants (humains ou non) ont muté et leurs organismes sont devenus dépendants à l’un de ces métaux, le plus fréquent, baptisé « Astra-Z ». Pendant un délai appelé « Temps de Tranquillité et de Liberté » (TTL), ils sont délivrés de tout besoin physiologique et peuvent faire ce que bon leur semble. Cependant, ceux qui n’arrivent pas à récupérer une nouvelle dose d’Astra-Z durant ce TTL perdent le qualificatif de « survivant ». De plus, la taille d’une météorite ne permet qu’à un seul individu de recharger son TTL et augmente sa dépendance, si bien qu’il lui faut trouver une nouvelle météorite en un TTL plus court que le précédent. Au final, les survivants passent donc la majorité de leur temps de tranquillité à traquer les chutes de météorites et à se précipiter sur les points d’impact pour y collecter la précieux métal.

La plupart des météorites contiennent de l’Astra-Z, mais certaines, plus rares, contiennent du « Astra-X », lequel est capable de guérir définitivement un survivant de sa dépendance à l’Astra-Z. D’autres enfin, contiennent du « Bêta-X », qui est en revanche mortel. La technologie actuelle permet facilement de détecter à distance les points d’impacts d’une météorite, ainsi que la présence d’Astra-Z, mais ne permet pas de discerner l’Astra-X du Bêta-X.

Tant qu’ils ne sont pas délivrés de leur dépendance à l’Astra-Z, les survivants sont maintenus à l’intérieur d’une Zone Rectangulaire de Ressources (ZRR) par un champ magnétique mis en place par le Gouvernement de la Terre, afin de les aider à partager les ressources équitablement. Tout individu sous l’emprise de l’Astra-Z qui essaye de franchir la limite de la zone qui lui est assignée ne fait plus partie de la population des survivants. Pour les inciter à rester en forme et à conserver des interactions sociales, chaque ZRR renferme un petit groupe de quelques survivants qui sont libres de s’organiser ou d’entrer en compétition pour partager les ressources.

Dans sa volonté d’améliorer le confort et de maximiser le bonheur de la population, le Gouvernement de la Terre a lancé un grand projet d’application Web mobile destiné à permettre à la population de la Terre de trouver plus facilement les points d’impact des météorites. La première phase, qui a consisté à équiper toute la population de terminaux mobiles géolocalisés, a déjà eu lieu. Il ne reste plus qu’à développer l’application.

Devant le succès mitigé de son application précédente, le Gouvernement de la Terre a mandaté un cabinet de conseil offshore pour lancer un appel d’offres à plusieurs startups, dont vous faites partie. Votre mission, si vous l’acceptez, sera de développer d’une part une API permettant au Gouvernement de la Terre de mettre à disposition de la population les informations dont il dispose, et d’autre part un client Web mobile permettant aux survivants de faire le meilleur usage de ces informations. Il est à noter que cette mission comporte, en plus de ces deux objectifs, une partie confidentielle qui vous sera dévoilée plus loin.

## API (TP3 & 4)

### Partie publique

Afin d’aider la population de la Terre à survivre dans les meilleures conditions, vous devez développer une API fournissant aux survivants les informations suivantes :

- Limites de leur ZRR
- Points d’impact des météorites
- Présence de minerai de métal Astra-Z, d’un autre minerai, ou absence de minerai (si l’impact a déjà été visité par un autre survivant).
- Positions des survivants de la ZRR
- Rappel du TTL

### Partie confidentielle

Afin d’aider le Gouvernement de la Terre à optimiser la qualité de vie de la population, il vous est également demandé de développer une API qui ne sera pas accessible à la population, pour fournir à ses membres la possibilité de :

- Fixer les limites des ZRR
- Fixer le nombre de survivants autorisés 
- Déclencher l’envoi d’une dose de minerai d’un type choisi à un emplacement aléatoire d’une ZRR, pour faire croire aux survivants que la pluie de météorites n’a pas cessé et que le Gouvernement ne contrôle pas la répartition des ressources
- Surveiller l’état (position, temps de vie restant) et l’évolution (historique des récupérations de minerai) de la population de chaque ZRR

## Client Web (TP 5 -> 8)

### Partie publique

Il vous est demandé de mettre à disposition de la population de la Terre un client aussi ergonomique et agréable à utiliser que possible, les informant avec précision de leur position, de celle des autres survivants de leur ZRR, ainsi que de celle des impacts de météorites (et si elle est connue, de leur composition). Ils pourront visualiser tout cela sur une carte montrant les limites de la ZRR, et surveiller en parallèle leur TTL. Pour pouvoir l’utiliser, ils devront s’identifier sur le serveur d’authentification centralisée de la Terre (que vous avez réalisé aux TPs 1 et 2).

### Partie confidentielle

En plus des fonctionnalités de ce client, vous devrez aussi réaliser une interface de consultation et de modification des données réservée aux membres du Gouvernement de la Terre pour permettre d’utiliser les fonctionnalités de la partie réservée de l’API. Bien entendu, seuls certains utilisateurs y auront accès à travers une interface "back-office" séparée du client mobile.
