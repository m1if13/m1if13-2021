# TP 6 - Framework côté client (suite)

## Objectifs

- Maitriser la logique réactive des frameworks JS modernes
- Prendre en main un store centralisé pour un framework en JS : Vuex
- Expérimenter les APIs de stockage de données en JS

## Pointeurs

- [Vue.js](https://vuejs.org/)
- [VueX](https://vuex.vuejs.org/)
  - [exemple simple](https://vuex.vuejs.org/guide/)
  - [principes](https://vuex.vuejs.org/guide/state.html)
  - [installation](https://vuex.vuejs.org/installation.html)
  - [code détaillé d'exemples vue2 et vuex3](https://github.com/vuejs/vuex/blob/dev/examples/)

## Application

Dans ce TP, vous poursuivrez la réalisation du client pour la partie publique du jeu. Cet énoncé part du principe que :

- la gestion des utilisateurs est disponible en HTTPS à partir de l'URL `https://proxy-tps-m1if13-2019.univ-lyon1.fr/XX/`
- l'API publique du jeu côté serveur fonctionne, et qu'elle est déployée via le serveur nginx sur la route `/api`
- l'interface "confidentielle" (d'administration) fonctionne également pour pouvoir configurer le jeu ; elle ne sera pas utilisée dans ce TP mais vous en aurez besoin pour tester.
- vous avez configuré votre application Vue, implémenté [vos premiers composants et votre routeur](../tp5/)

Nous allons utiliser [vue cli](https://cli.vuejs.org/) et les [devtools](https://github.com/vuejs/vue-devtools) installés lors du TP précédent.

## 1. Mise en place du store VueX

### 1.1. Démarrage avec Vuex

Vuex fournit un espace centralisé pour gérer l'état des composants de votre application, et ainsi faciliter leur partage.

Installez le module vuex (avec vue cli): `vue add vuex`

Maintenant nous allons créer un `store` en suivant une [structure de projet similaire à celle ci-dessous](https://vuex.vuejs.org/fr/guide/structure.html) :

```
├── index.html
├── main.js
├── App.vue
├── api/
│   └── ...               # abstraction pour faire des requêtes par API
├── components/
│   └── ...
└── store/
    ├── index.js          # là où l'on assemble nos modules et exportons le store
    ├── actions.js        # actions de base
    ├── mutations.js      # mutations de base
    └── modules/          # modules optionnels
```

Pour le TP précédent et celui ci, regardez l'exemple du [Shopping Cart (Vue2+Vuex3)](https://github.com/vuejs/vuex/tree/dev/examples/shopping-cart)

### 1.2. Création du store :

L'état va contenir les propriétés suivantes :

- les informations du joueur :
  - identifiant et icone (ces informations seront persistées en plus en localstorage).
  - les coordonnées du joueur, pour ce TP on les simulera avec des clics de la souris), on utilisera le GPS dans les prochains TPs,
  - le dernier TTL initial du joueur (envoyé par le serveur),
- les limites de la ZRR (envoyées par le serveur),
- les positions des météorites (envoyées par le serveur),
- les informations (identifiants, positions, icone) des autres joueurs (envoyées par le serveur).

[Deux principes importants](https://vuex.vuejs.org/fr/guide/) à retenir sur les stores Vuex :

> 1. Les stores Vuex sont réactifs. Quand les composants Vue y récupèrent l'état, ils se mettront à jour de façon réactive et efficace si l'état du store a changé.
>
> 2. Vous ne pouvez pas muter directement l'état du store. La seule façon de modifier l'état d'un store est d'acter (« commit ») explicitement des mutations. Cela assure que chaque état laisse un enregistrement traçable, et permet à des outils de nous aider à mieux appréhender nos applications.

```js
import { createStore } from 'vuex'
import game from '../../api/game'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

const store = createStore({
  state () {
    return {
      zrr: [[x0,y0], [x1,y1]],
      playerPosition : [x, y],
      ...
    }
  },
  mutations,
  actions,
  getters,
})

export default store;
```

### 1.3 Création des mutations :

Les mutations sont les transformation atomiques effectuées sur l'état de votre application.
Pour chaque élément du store créez les mutations correspondantes. Elle suivent souvent les principes de C(R)UD: ajout, suppression, modification. Il s'agit de modification alors pas de nécessité d'avoir la lecture (pas de read), vue se chargera de diffuser les changements d'état aux composants.

On voudra (liste non exhaustive, pour vous guider) :

- créer la ZRR
- créer, mettre à jour la position d'un joueur
- créer et modifier le TTL donné par le serveur
- définir les positions des météorites
- définir la liste des joueurs et leurs propriétés associées. Mettre à jour leurs positions.

Pour s'assurer que les mutations sont bien propagées à l'état de vue. On utilisera : `Vue.set( ... )` avec ... correspondant à l'état modifié.

### 1.4 Création des actions :

Nous allons une première action qui va artificiellement modifier la position du joueur. Pour cela ajoutez un EventListener sur le composant contenant la carte.

Les autres actions sont liées à des changements côtés serveur. En regardant l'exemple du [chat](https://github.com/vuejs/vuex/tree/dev/examples/chat) côté `api/` et `store/` vous pourrez voir comment mettre à jour l'état après une requête à un serveur.

Pour ce TP, nous allons utiliser un bouton `mise à jour` pour lancer la requête au serveur. Nous mettrons en place une stratégie de polling par la suite.

### 1.5. Création des getters :

Si Vue diffuse bien les changements d'état, il peut être utile de fournir à l'application un état dérivé d'un autre état. Par exemple le TTL courant à partir du dernier TTL envoyé par le serveur (gardez le timestamp de sa création, revoyez le delta suivant TTL - (le timestamp courant - timestamp de création). Ou la distance entre un joueur et une météorite. On utilise pour cela [des getters](https://vuex.vuejs.org/guide/getters.html).

Une fois les getters définis, ils sont accessibles depuis l'objet `store`.

Ne créez des getters que si vous en ressentez le besoin. Ce n'est pas obligatoire.

### 2. Liens entre Store et Composants

Nous allons maintenant utiliser le data-flow réactif pour mettre à jour les composants (au lieu des événements et du bus du TP précédent).

Commencez par connecter l'état de vos composants au store. Voir par exemple le cas de cette [todo liste](https://github.com/vuejs/vuex/blob/dev/examples/todomvc/).

**Attention la syntaxe change un peu entre Vue2 et Vue3.**

### 3. Fonctionnalités à réaliser

Nous allons reprendre les objectifs du TP précédent mais cette fois utilisant en le store vuex.

- Sur la carte, faites apparaître les limites de la ZRR (autour du Nautibus) et remplacez le marker par l'icone du joueur courant. Stockez dans le store :
  - la limite de la ZRR,
  - les coordonnées du joueur,
  - le TTL initial du joueur,
  - la position des météorites.
- Utilisez l'API Web storage pour stocker le token renvoyé par le serveur Spring, ainsi que les informations de base de l'utilisateur (identifiant, URL de l'image représentant l'icone de l'utilisateur sur la carte).
- Renvoyez les coordonnées du joueur au serveur Express toutes les 5 secondes.
- Récupérez la réponse du serveur avec éventuellement, la position d'une météorite et le TTL de l'utilisateur. Affichez la météorite sur la carte et le TTL quelque part sur l'écran.
- Lorsque le TTL est expiré ou lorsque l'utilisateur a rejoint une météorite (à moins de 2m), faites en sorte que l'affichage affiche un nouveau composant textuel par-dessus la carte, avec les informations correspondantes.
