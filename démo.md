## Préparation de la démo (à faire avant)

- Si vous avez besoin d'un utilisateur admin pour faire fonctionner la partie "confidentielle" de l'interface , créez-le.
- Créez deux utilisateurs "survivants" (joueurs normaux) :
  - un que vous utiliserez pour la démo
  - un que les enseignants utiliseront pour visualiser le point de vue d'un joueur ; les credentials seront prof/prof
- Si vous n'avez pas réalisé le client admin : utilisez un client simple de type Postman pour réaliser tout ce qui est indiqué dans "Partie Confidentielle" **avant la démo**.

## Matériel à apporter

- un PC portable (avec batteries) pour le client admin
- un smartphone pour le client "normal" / PWA

De notre côté, nous testerons avec nos téléphones le 2ème utilisateur en //.

## Déroulement de la démo

### Présentation des APIs

**2 minutes max**

Quelques informations à nous donner au début de la démo.

- Spring :
  - montrer l'API Swagger déployée
  - indiquer si la démo utilise votre API ou celle que nous avons mise à disposition (machine 192.168.75.80)
- Express :
  - Indiquer s'il y a des choses que vous avez développées en plus, ou s'il y a des fonctionnalités manquantes par rapport au pitch du jeu

### Démo

#### Partie confidentielle (= client admin, si présent)

**3 minutes**

  - Positionner la ZRR (périmètre du jeu)
  - Y placer 2 survivants
  - Choisir un minerai
  - Déclencher l'envoi d'une météorite
  - Observer le déroulé du jeu sur l'interface

#### Partie publique (= Client "normal")

**5 minutes**

  - Se loguer
  - Visualiser la carte avec les limites de la ZRR, la position des joueurs et les impacts de météorites
  - Visualiser le TTL en train de décroître
  - Atteindre une météorite avant la fin du TTL
  - Laisser le TTL se terminer sans atteindre de météorite...

### Questions (éventuellement)
