# TP 3 - JavaScript côté serveur

## Objectifs

Comprendre le rôle et utiliser quelques outils de gestion de projet pour mettre en place un serveur en JS

  - NPM : gestion de dépendances côté serveur
  - Node : exécution côté serveur
  - Express : serveur Web

## Pointeurs

Documentation et tutos :

  - NPM :
    - [Documentation](https://docs.npmjs.com/)
    - [CLI](https://docs.npmjs.com/cli/v7/commands)
  - Node :
    - [Guides et tutos](https://nodejs.org/en/docs/guides/)
    - [Documentation de l'API](https://nodejs.org/api/) (V. 15)
  - [Express](https://expressjs.com/) :
    - [lancer un serveur](https://expressjs.com/en/starter/hello-world.html)
	  - [servir des fichiers statiques](https://expressjs.com/en/starter/static-files.html)
	  - [Routage (répondre à des URLs spécifiques)](https://expressjs.com/en/guide/routing.html)
	  - [Traiter des données de formulaire](https://www.npmjs.com/package/body-parser)
  - ESLint :
    - [installation](https://eslint.org/docs/user-guide/getting-started)
    - [configuration]()
    - [règles](https://eslint.org/docs/rules/)
 
## Description de l'application

Dans ce TP et les suivants, vous allez travailler sur une application que vous transformerez progressivement en un jeu Web mobile, multi-joueurs, où les utilisateurs seront géolocalisés et utiliseront une visualisation cartographique. Vous trouverez à la racine de ce projet le [pitch de ce jeu](../pitch.md).

Ce TP sera consacré à la mise en place de l'API côté serveur du jeu. &Agrave; la fin de ce TP :
- les joueurs devront pouvoir s'identifier, accéder aux informations exposées par le serveur (limites de la ZRR, coordinnées des points d'impact et des autres joueurs, composition des météorites) et remonter au serveur leur position et leur TTL.
- un joueur administrateur pourra spécifier les limites d'une ZRR, déclencher l'impact d'une météorite, et accéder aux informations remontées par les autres joueurs.

## Travail à réaliser

### 1. Initialisation

  - Créez un dossier `api` à la racine de votre dépôt de code. dans lequel vous placerez ce qui concerne le serveur Express
  - Dans ce dossier, créez un projet NPM à l'aide de [ce tuto](https://docs.npmjs.com/creating-a-package-json-file)<br>
    Attention : utilisez la commande `npm init` sans l'option `--yes` pour pouvoir entrer autre chose que les valeurs par défaut.
  - &Agrave; l'aide de [ce template](https://github.com/github/gitignore/blob/master/Node.gitignore), complétez le fichier `.gitignore` de votre dépôt avec les noms de fichiers et répertoires à ignorer pour votre projet (_a minima_ le répertoire `node_modules` et les fichiers de votre IDE).
  - **Avant de pusher sur la forge, vérifiez bien que le répertoire node_module n'est pas inclus dans votre `git status`.**
  - Installez (en dépendance de dev) *ESLint* dans votre projet, et configurez-le avec les options par défaut, sauf :
    - `Which framework does your project use?` -> `None of these`
    - `Where does your code run?` -> `Node`
    - `What format do you want your config file to be in?` -> `JSON`
    <br>
    Dans le fichier `.eslintrc.json` généré, rajoutez *a minima* une [règle](https://eslint.org/docs/rules/) pour spécifier que l'indentation doit être de 4 espaces 

## 2. Partie serveur

Faites une page d'accueil HTML basique pour tester que vous pouvez requêter voter serveur Express.

### 2.1. Serveur de fichiers statiques

Vous allez utiliser [Express](https://expressjs.com/) pour le back-office de votre application.

- Ajoutez à votre projet une dépendance vers la dernière version d'Express
- Utilisez les tutos sur Node / Express pour démarrer un serveur Web (en mode Hello World).
- Faites en sorte que votre serveur Express réponde aux requêtes sur `/static` en servant directement les fichiers situés dans le répertoire `public`.
- Ajoutez un [middleware de gestion des erreurs 404](http://expressjs.com/en/starter/faq.html#how-do-i-handle-404-responses)
- Testez.

### 2.2. Contenus dynamiques

Dans cette partie, vous ne mettrez en place qu'une API côté serveur. Cette API va gérer les ressources géolocalisées : les utilisateurs et d'autres éléments placés aléatoirement par le serveur. 

Voici les différents types de ressources gérées par le serveur (fournis à titre indicatif) :

```
ZRR
  |__limite-NO
  |__limite-NE
  |__limite-SE
  |__limite-SO
user
  |__survivant
  |  |__image
  |  |__position
  |  |__TTL
  |__administrateur
meteorite
  |__impact
  |  |__position
  |__composition
```

Vous allez mettre en place 2 parties de votre application capables de [servir des contenus dynamiques](http://expressjs.com/en/starter/basic-routing.html), une pour gérer le fonctionnement du jeu, et une autre pour l'interface d'administration.

Pour chacune de ces 2 parties de l'application, vous créerez un middleware spécifique, à l'aide de la classe [`express.Router`](http://expressjs.com/en/guide/routing.html#express-router), et l'associerez à une route spécifique (voir partie Déploiement). Vous placerez le code de ces routes dans un répertoire `routes`.

#### 2.2.1. Gestion du fonctionnement du jeu

Les accès aux ressources se feront ainsi :
- mise à jour de l'URL de l'image associée à l'utilisateur (et qui sera géolocalisée sur la carte)
- mise à jour de la position de l'utilisateur
- récupération de la liste complète des objets (autres utilisateurs, impacts) à afficher

L'API du serveur est donnée dans le fichier [`express-api.yaml`](./express-api.yaml). Vous pouvez vous aider de la génération de serveurs pour générer le squelette de votre code.

&Agrave; la réception de chaque requête, n'oubliez pas de valider l'identité de l'utilisateur avec le serveur Spring :

- pour le requêtage du serveur Spring, vous pouvez utiliser [Axios](https://www.npmjs.com/package/axios)
- pour éviter de vous e...er avec la configuration d'Axios lorsque vous requêtez le serveur Spring (dont le certificat est signé par une autorité qu'il ne connaît pas), vous pouvez le requêter sur le port 8080 plutôt qu'en passant par nginx.

#### 2.2.2. Interface d'administration

&Agrave; vous de spécifier et d'implémenter l'API et générer les pages permettant de réaliser les fonctionnalités ci-dessous.

Au moment du paramétrage d'une partie, cette interface d'administration devra permettre de :
  - fixer le périmètre de jeu (par exemple, en spécifiant 2 points, et en choisissant un rectangle dont ces points sont les coins)
  - préciser le `ttl` initial (valeur par défaut : 1 minute)
  - Démarrer la partie

Une fois la partie démarrée, cette interface permettra de :
  - déclencher la chute d'une météorite (d'un type donné)
  - suivre la position et le TTL des joueurs
  - indiquer quand et par qui une météorite a été récupérée

**Indication** : plus de détails sur l'implémentation de cette interface sont donnés au [TP4](../tp4/README.md)

## 3. Déploiement

Configurez votre serveur Express pour qu'il réponde sur le port 3376. Configurez votre serveur Express pour avoir les routes ci-dessus :

- `/api` pour la partie publique
- `/admin` pour la partie confidentielle
- `/public` pour les contenus statiques (pour faciliter les tests)

Normalement, vous devez pouvoir interroger votre serveur Express sur le port 3376 (port ouvert sur le firewall de l'OpenStack).

**Aide** : pour faciliter l'installation d'Express en tant que service sur votre VM, vous pouvez utiliser le package [PM2](https://www.npmjs.com/package/pm2).

Comme au premier semestre :

- Configurez votre serveur nginx en HTTPS ; vous trouverez les certificats dans le sous-répertoire `Certificats` de la [page d'accueil de l'UE](https://perso.liris.cnrs.fr/lionel.medini/enseignement/M1IF13/).
- Configurez votre serveur nginx en reverse proxy ; déployez la partie serveur de votre projet sur une route `/game` de votre proxy nginx.

## Date de rendu

Ce TP doit être déployé sur votre VM au plus tard lundi 15 mars 2021 à 23h59.
