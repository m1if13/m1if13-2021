# TP 7 - Web Mobile

## Objectifs

  - Adapter votre application web pour des vues mobiles
  - Accéder aux capteurs du téléphone
  - Terminer l'implémentation de votre jeu

## Pointeurs

  - [Les règles du jeu](https://forge.univ-lyon1.fr/m1if13/m1if13-2021/-/blob/master/pitch.md)
  - Capteurs
    - [Simuler sa localisation](https://developers.google.com/web/tools/chrome-devtools/device-mode/geolocation)
    - [LocationGuard](https://github.com/chatziko/location-guard)
    - [Generic Sensor Demos](https://intel.github.io/generic-sensor-demos/) à tester avec différents dispositifs et navigateurs.
  - Tester 
    - [Simulation mobile des Chrome Dev Tools](https://developers.google.com/web/tools/chrome-devtools/device-mode)
    - [Remote debugging toujours avec les Chrome Dev Tools](https://developers.google.com/web/tools/chrome-devtools/remote-debugging)
    - [Responsive Design Mode de Firefox](https://developer.mozilla.org/en-US/docs/Tools/Responsive_Design_Mode)

## Application

Dans ce TP, vous continuerez de travailler sur l'application des TPs précédents. Cet énoncé part du principe que le client et l'API côté serveur fonctionnent :

- Votre client doit être packagé en webpack et déployé sur nginx.
- Votre client est une application Vue.js avec gestion des états et un routeur (côté client) qui permette de gérer les différentes vues de votre application.
- Une route côté serveur doit permettre d'accéder à l'API contenant le métier de l'application pour les joueurs "normaux".

## Travail à réaliser

Commencez par (re)lire les [règles du jeu](https://forge.univ-lyon1.fr/LIONEL.MEDINI/m1if13-2021/-/blob/master/pitch.md) pour être au clair sur la logique de l'application.


### Capteurs et géolocalisation

Dans un premier temps, faites en sorte de récupérer la position de l'utilisateur grâce à la [Geolocation API](https://www.w3.org/TR/geolocation-API/). Déployez un script simple sur votre VM, et sortez pour vérifier que cela correspond bien à votre position.

De retour chez vous :

- mettez en place la récupération de la position dans votre jeu
- faites patienter l'utilisateur tant que sa position n'est pas présente (affichez une erreur au bout d'une minute)
- remplacez la position mockée par celle récupérée depuis le GPS du device

**Aide :** cela peut être simulé par différents client ouverts en même temps. Voir les outils de [simulation de localisation](https://developers.google.com/web/tools/chrome-devtools/device-mode/geolocation) listés plus haut.

Plutôt que d'interroger le GPS toutes les 5 secondes, découplez l'utilisation du capteur du reste de l'application à l'aide du store VueX :

- récupérez la position courante et utilisez [`watchPosition`](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/watchPosition) pour être notifié d'un changement de position
- à l'aide d'actions et de mutations, tenez le store à jour de la position de l'utilisateur
- utilisez les informations stockées dans le store pour metrte à jour la carte et envoyer la position au serveur.
- vérifiez que les positions des différents joueurs s'affichent sur la carte

### Vibreur

Une vue spéciale (modale) permettra d'afficher le fait que l'utilisateur local vient de gagner ou perdre la partie

Faire [vibrer le téléphone](https://developer.mozilla.org/en-US/docs/Web/API/Vibration_API) en cas de fin de partie. 


### En bonus

#### Thème et luminosité
Nous allons changer le thème de l'appliation en fonction de la [luminosité ambiante](https://www.w3.org/TR/ambient-light/) pour pouvoir jouer dans le noir.

Pour cela il faut autoriser l'accès aux generic sensors : [chrome://flags/#enable-generic-sensor-extra-classes](chrome://flags/#enable-generic-sensor-extra-classes)

#### Direction des joueurs

Quand les joueurs bougent, indiquer leur direction. Plusieurs approches sont possibles:

- calculer la direction en se basant sur la position courante et la position passée 
- utiliser la boussole (le magnétomètre) du téléphone. Le magnétomètre est protégé pour des raisons de protection de la vie privée. Il faut également activer l'[accès aux generic sensors](chrome://flags/#enable-generic-sensor-extra-classes) dans les options de Chrome.
- d'autres approches s'appuyant sur les valeurs de l'accéléromètre sont possibles.

## Rendu

Ce TP devra avoir été pushé sur votre dépôt forge pour le **lundi 10 mai 2021** à 23h59, **avec le tag TP7**.




