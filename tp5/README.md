# TP 5 - Framework côté client

## Objectifs

  - Prendre en main un framework en JS : Vue.js
  - Expérimenter les APIs de stockage de données en JS

## Pointeurs

  - [Vue.js](https://vuejs.org/)
  - [Vue-cli](https://cli.vuejs.org/)
  - [Vue-router](https://router.vuejs.org/)
  - API Web Storage :
    - [Spec](https://html.spec.whatwg.org/multipage/webstorage.html#webstorage) (HTML Living Standard)
    - [Doc](https://developer.mozilla.org/fr/docs/Web/API/Web_Storage_API) (MDN)
 
## Application

Dans ce TP, vous allez commencer à réaliser le client pour la partie publique du jeu. Cet énoncé part du principe que :
- la gestion des utilisateurs est disponible en HTTPS à partir de l'URL `https://proxy-tps-m1if13-2019.univ-lyon1.fr/XX/` 
- l'API publique du jeu côté serveur fonctionne, et qu'elle est déployée via le serveur nginx sur la route `/api`
- l'interface "confidentielle" (d'administration) fonctionne également pour pouvoir configurer le jeu ; elle ne sera pas utilisée dans ce TP mais vous en aurez besoin pour tester.

**Remarque** : dans ce TP, vous ne travaillerez pas avec de "vraies" données. Ce TP va vous permettre de mettre en place un framework, mais la gestion des données sera mise en place au TP6 sur le _State Management Pattern_ Vue. Dans ce TP, **vous travaillerez avec des données mockées**.

## Outils

- Vous allez utiliser **Vue CLI** pour créer votre projet. Suivez les instructions [ici](https://cli.vuejs.org/guide/installation.html) pour l'installer.
- Pour faciliter le développement/debug, il est conseillé de - installer le plugin [**Vue.js devtools**](https://github.com/vuejs/vue-devtools) dans votre navigateur. Attention, choisissez le plugin conçu pour la version 3 de Vue, car celui pour la version 2 ne détectera pas le framework dans votre application.

## 1. Mise en place de Vue.js

### 1.1. Initialisation avec Vue-CLI

Lancez la commande `vue ui` (votre navigateur doit s'ouvrir à l'URL http://localhost:8000/) et laissez-vous guider par l'interface Web de Vue CLI. Créez un projet Vue avec les caractéristiques suivantes :
- Nom du sous-répertoire de votre dépôt : `client`
- Preset : `Manual`
- Features à activer :
  - Choose Vue version
  - Babel
  - Linter / Formatter
  - Use config files
  - _optionnel : si vous avez aimé TypeScript au TP précédent, vous pouvez l'utiliser aussi_
- Configuration :
  - Version de Vue : "3.x (Preview)"
  - linter / formatter config : "ESLint + Prettier"
  - "Lint on save" & "Lint and fix on commit" : activés

Vous pouvez enregistrer ces réglages (_presets_) au cas où vous auriez à recommencer.

Normalement, vous devriez voir votre projet dans le "Vue Project Manager" accessible par l'icone en forme de maison en bas à gauche.

Pour économiser des ressources, vous pouvez fermer Vue-CLI UI pour l'instant.

### 1.2. Première application Vue

Ouvrez un IDE dans le répertoire du projet. Commencez par regarder le contenu du fichier `package.json`.

Depuis un terminal, placez-vous dans le répertoire de votre projet Vue et lancez le script `serve`. Si ça ne se fait pas automatiquement, ouvrez un onglet à l'URL http://logalhost:8080 (remarque : vérifiez avant que rien ne tourne sur ce port).

Depuis votre IDE, changez le contenu du composant HelloWorld. Après quelques secondes, vous derviez voir la page se modifier dans votre navigateur. Dans les Dev tools, vous devriez avoir un nouvel onglet "Vue" qui vous informe sur l'application, les composants etc.

### 1.3. Configuration de l'application

&Agrave; la racine de votre projet, créez un fichier `vue.config.js`.

Dans ce fichier :
- commencez par changer le port du serveur de dev (pour éviter les conflits avec Spring Boot par exemple) : https://cli.vuejs.org/config/#devserver
- rajoutez à l'objet JSON exporté un sous-objet `configureWebpack` comme indiqué ici : https://cli.vuejs.org/guide/webpack.html#simple-configuration Pour l'instant, laissez-le vide, mais vous pourrez avoir à l'utiliser (comme vous avez utilisé `webpack.config.js` au TP précédent) plus tard.

Arrêtez et relancez le serveur de dev pour mrendre en compte le changement de port.

### 2. Composants

Comme vous l'avez vu dans le projet généré, une application Vue est une arborescence de composants, dont la racine est celui lancé dans le fichier `main.js`. Chaque composant Vue contient une partie de votre application, avec un template, un comportement (script) et un style.

Pour vous faire gagner du temps, nous vous donnons [le composant qui permet d'affichier la carte avec Leaflet](./Map.vue).

&Agrave; vous de créer / modifier les composants qui vont permettre de réaliser les autres fonctionnalités (page d'accueil de l'application, formulaire de login, mise à jour des autres données).

**Pour l'instant, affichez tous ces composants les uns au-dessous des autres. Il est fortement conseillé de faire un push sur la forge à la fin de cette partie.**

  **Aide** :
  - pour pouvoir inclure des composants à un niveau quelconque de l'arborescence DOM, il faut les déclarer explicitement dans l'application Vue, à l'aide de la propriété `components` : https://stackoverflow.com/questions/39382032/vue-js-unknown-custom-element
  - pour communiquer programmatiquement en 2-way binding entre un composant et la racine de l'application, ou entre plusieurs composants applicatifs, il faut utiliser les [Custom Events](https://vuejs.org/v2/guide/components-custom-events.html) de vue. Un bon exemple dans [ce tutoriel](https://flaviocopes.com/vue-components-communication/#using-an-event-bus-to-communicate-between-any-component). Ainsi, vous pouvez donc définir :
    - des événements "globaux" (event bus) accessibles par toute l'application,
    - des événements "locaux" liés à un sous-arbre de composants, en les "bindant" au composant de plus haut niveau souhaité : `this.$on()` dans ce composant.
    - quand vous rajoutez dynamiquement des éléments HTML à des composants dans des fichiers .vue, ils ne peuvent pas utiliser les styles définis pour votre composant, car ces styles sont "scoped" -> il faut rajouter l'attribut `data` du composant avec `vm.$options._scopeId` (voir [ici](https://github.com/vuejs/vue-loader/issues/559)).


### 3. Routeur

Dans cette partie, vous allez transformer votre application en Single-Page Application en utilisant le routeur de Vue.

**Remarque** : le routeur de Vue permet de faire du routage côté client, même si les paths commencent par un slash (les URLs sont en http://.../#/route).

- Redémarrez `vue ui` et accédez à votre projet dans l'interface Web. Dans la page "plugins", cliquez sur `add router`. Attendez la fin de l'opération et fermez Vue CLI.
- Rechargez votre application et observez les modifications du composant principal et le contenu du dossier `router`.
- Modifiez le menu et la configuration du routeur pour que votre arborescence s'affiche comme une SPA.

**Aide** : contrairement à l'exemple simple qui vous a été présenté en M1IF03, le routeur de Vue ne se contente pas de changer les propriétés CSS des éléments qui ne sont pas dans la route courante pour ne pas qu'ils soient visibles. Il les supprime du DOM et les met en les "cache". En conséquence, certaines parties de vos composants ne seront pas accessibles quand les composants sont "arrêtés" et les hooks liés à leur cycle de vie sont rappelés quand vous revenez sur la route à laquelle ils appartiennent. De la même façon, un composant n'a pas été initialisé tant que sa route n'a pas été activée. Cela pose plusieurs problèmes :

  - Leaflet n'est pas prévu à l'origine pour être affiché dans un composant Vue, et __a fortiori__ pas dans une SPA avec un routeur qui "éteint" les composants quand ils ne sont pas affichés. Il faudra prévoir de créer puis de replacer la map aux bonnes coordonnées et au bon niveau de zoom chaque fois que la route change et qu'elle doit être réaffichée.
  - Si certains éléments comme les rectangles sont automatiquement réaffichés avec la map (vous pouvez donc les ajouter avec `mapTo()` et ne plus vous en préoccuper), les markers ne fonctionnent pas de la même façon (non documenté). Pour afficher des markers "routing-proof", vous devrez donc :
    - mémoriser (localement ou dans le store) les données de chacun des markers
  - recréer ces markers et les ajouter à la map à chaque nouvel affichage (hook : `bounded`) du composant
  - supprimer ces markers et les retirer de la map à chaque changement de route et arrêt (hook : `beforeDestroy`) du composant
  - Apparemment, les bindings des événements dans Vue ne sont pas gérés de la même façon en fonction de leur target :
    - les bindings aux événements globaux (Event bus) sont conservés entre les créations du composant, même si ce composant est démarré et arrêté par le routeur. Donc si vous faites un binding dans un hook (`created` ou `mounted`), vous déclencherez autant de fois le callback que vous aurez redémarré le composant. Solution : mettre dans le module un booléen qui vérifie que les bindings n'ont été faits qu'une fois.
  - les bindings des événements locaux au composant sont perdus quand on change de route. Il faut donc bien les définir dans le hook `mounted`.

## 4. Fonctionnalités à réaliser

&Agrave; vous de faire en sorte que votre application soit fonctionnelle :

- Sur la carte, faites apparaître les limites de la ZRR (autour du Nautibus) et remplacez le marker par l'icone du joueur courant (on restera en mono-joueur dans ce TP). Stockez l'ensemble de ces données (limite de la ZRR, coordonnées et URL de l'icone du joueur dans un objet JSON constant).
- Utilisez l'API Web storage pour mémoriser ces informations, ainsi que le token renvoyé par le serveur Spring.
- Faites comme si les coordonnées de l'utilisateur étaient variables, et renvoyez-les au serveur Express toutes les 5 secondes.
- Récupérez la réponse du serveur avec éventuellement, la position d'une météorite et le TTL de l'utilisateur. Affichez la météorite sur la carte et le TTL quelque part sur l'écran.
- Lorsque le TTL est expiré ou lorsque l'utilisateur a rejoint une météorite (à moins de 2m), faites en sorte que l'affichage affiche un nouveau composant textuel par-dessus la carte, avec les informations correspondantes.

**Aide** : vous pouvez réaliser ces fonctionnalités dans les scripts des composants (méthodes ou hooks), ou bien les placer dans des fichiers JS à part si elles doivent être partagés par plusieurs composants.
